package searching;

import java.util.Random;

/*
 * ArraySize = 50,   Mean = 31,   Ratio = 0.6200
 * 			 = 100,    	  = 64,   		= 0.6400
 * 			 = 500,  	  = 315,  		= 0.6400
 * 			 = 1000, 	  = 631,  		= 0.6310
 * 			 = 5000, 	  = 3156, 		= 0.6312
 * 
 * 	AverageRatio = 0.63
 * 
 * 	Therefore, on average, 63% of the array must be searched
 *  to find desired number. This percentage includes when the
 *  whole array is searched, and the desired value is NOT found.
 *  If that was not included, then the average percentage would be 50%.
 */
public class LinearSearchEfficiency {
	int comparisons = 0;

	public static void main(String[] args) {
		LinearSearchEfficiency ls = new LinearSearchEfficiency();
		int arraySize = 1000;
		int[] array = new int[arraySize];

		int sum = 0;
		int numberOfLoops = 5000;
		for (int i = 0; i < numberOfLoops; ++i) {
			ls.comparisons = 0;
			ls.initializeRandom(array);

			ls.search(array, 42);
			sum += ls.comparisons; // Add comparisons to sum

		}

		// why is numberOfLoops cast to a double?
		/*
		 * numberOfLoops must be casted to a double in order to calculate the decimals
		 * of the value. Without casting, numberOfLoops is an integer, with zero decimal
		 * places. This means that zero decimal places are calculated. Casting to a
		 * double allows decimal places, so the decimal values are calculated.
		 */
		double mean = sum / (double) numberOfLoops;
		System.out.println(sum + " comparisons performed in " + numberOfLoops + " loops");
		System.out.println("mean value is " + mean);

	}

	// search for key within array
	// return the index of the key if key is in array
	// return -1 if key is not in array
	public int search(int[] array, int key) {
		for (int i = 0; i < array.length; i++) {
			this.comparisons++;
			if (key == array[i]) {
				return i;
			}
		}
		return -1;
	}

	private void initializeRandom(int[] array) {
		Random r = new Random();
		for (int i = 0; i < array.length; i++) {
			array[i] = r.nextInt(array.length) + 1;
		}
	}

	public int getComparisons() {
		return comparisons;
	}

	public void setComparisons(int comparisons) {
		this.comparisons = comparisons;
	}
}
