package searching;

import java.util.Random;
import java.util.Scanner;

@SuppressWarnings("unused")
public class LinearStringSearch {

	String[] array;
	int stringLength = 5;

	public static void main(String[] args) {
		// Create a new instance of the class LinearStringSearch.
		LinearStringSearch ls = new LinearStringSearch();

		// Create a keyboard scanner
		Scanner keyboard = new Scanner(System.in);

		// Grab the next integer from the keyboard
		System.out.println("Enter Array Length: ");
		int N = keyboard.nextInt();

		// consume the new line character
		keyboard.nextLine();

		// call the initializeRandom method
		ls.initializeRandom(N);

		// print out the string array
		ls.dump();

		// get the string to search for from the keyboard
		System.out.println("Enter Search Key: ");
		String searchString = keyboard.nextLine();
		
		// Close keyboard
		keyboard.close();

		// Call the find method of the LinearStringSearch class.
		// Put the return value of the find method into
		// the variable idx.
		int idx = ls.findWhile(searchString);
		if (idx == -1) {
			System.out.println(searchString + " not found in array.");
		} else {
			System.out.println(searchString + " found in array at index = " + idx);
		}
	}

	/*
	 * Create an array of strings. Set the array elements to be random strings of
	 * length stringLength, consisting of the characters A to Z.
	 */
	private void initializeRandom(int N) {
		array = new String[N];
		Random ran = new Random();
		for (int i = 0; i < array.length; ++i) {
			String randomString = "";
			int randStringLength = ran.nextInt(stringLength) + 1;
			for (int j = 0; j < randStringLength; ++j) {
				char c = (char) (ran.nextInt(26) + 65); // 65 is equal to 'A', and there are 26 letters in the alphabet,
														// so 65 + 26 is equal to 'Z'. Therefore generates random
														// character from 'A' to 'Z'
				randomString += c;
			}
			array[i] = randomString;
		}
	}

	private int find(String searchString) {
		int index = -1;

		for (int i = 0; i < array.length; i++) {
			if (array[i].equals(searchString)) {
				index = i;
				break;
			}
		}
		return index;
	}

	private int findWhile(String searchString) {
		int index = -1;
		int arrayIdx = 0;

		while (index == -1 && arrayIdx < array.length) {
			if (array[arrayIdx].equals(searchString)) {
				index = arrayIdx;
			}
			arrayIdx++;
		}
		return index;
	}

	private void dump() {
		for (int i = 0; i < array.length; ++i) {
			System.out.print(array[i] + " ");
		}
		System.out.println();
	}

}
