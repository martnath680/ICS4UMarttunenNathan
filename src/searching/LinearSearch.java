package searching;

import java.util.Random;
import java.util.Scanner;

public class LinearSearch {

	public static void main(String[] args) {
		LinearSearch ls = new LinearSearch();
		Scanner keyboard = new Scanner(System.in);
		
		// get arraySize from keyboard
		int arraySize = ls.getIntFromKeyboard("arraySize", keyboard);

		int[] array = new int[arraySize];

		ls.initializeRandom(array);
		ls.dump(array);

		// get searchInt from keyboard
		int searchInt = ls.getIntFromKeyboard("searchInt", keyboard);
		keyboard.close();
		
		int idx = ls.find(array, searchInt);
		if (idx == -1) {
			System.out.println(searchInt + " not found in array.");
		} else {
			System.out.println(searchInt + " found in array at index = " + idx);
		}
	}

	private int getIntFromKeyboard(String s, Scanner keyboard) {
		System.out.println("Enter Value for: " + s);
		int nextInt = keyboard.nextInt();
		keyboard.nextLine(); // Consume new line character
		return nextInt;
	}

	public void initializeRandom(int[] array) {
		Random r = new Random();
		for (int i = 0; i < array.length; i++) {
			array[i] = r.nextInt(array.length) + 1;
		}
	}

	// look for searchInt in array.
	// return index of searchInt in array
	// otherwise return -1 if searchInt is not in array
	public int find(int[] array, int searchInt) {
		// initialize
		int index = -1;

		for (int i = 0; i < array.length; i++) {
			if(array[i] == searchInt) {
				index = i;
				break;
			}
		}
		return index;
	}

	// print out the array
	public void dump(int[] array) {
		for (int i = 0; i < array.length; ++i) {
			System.out.print(array[i] + " ");
		}
		System.out.println();
	}

}
