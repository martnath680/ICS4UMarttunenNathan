package searching;

public class BinarySearch {
	private int comparisons = 0;

	public int getComparisons() {
		return comparisons;
	}

	public void setComparisons(int comparisons) {
		this.comparisons = comparisons;
	}

	public int search(int[] array, int key, int low, int high) {
		this.comparisons++;
		if (low > high) {
			return -1;
		}
		int midIdx = (int) Math.floor((low + high) / 2);
		if (array[midIdx] == key) {
			return midIdx;
		} else if (array[midIdx] > key) {
			return search(array, key, low, midIdx - 1);
		} else {
			return search(array, key, midIdx + 1, high);
		}
	}
}
