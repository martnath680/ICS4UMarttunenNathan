package keyboardinput;

import java.util.Scanner;

//TODO: Fix the bug

public class GetDate {

	static Scanner keyboard;
	static String dateS;
	static int[] dateI;

	public static void main(String[] args) {
		keyboard = new Scanner(System.in);
		GetDate gd = new GetDate();

		dateS = gd.getDate();
		dateI = gd.splitDate(dateS);
		gd.printDate(dateI);
	}

	private String getDate() {
		System.out.println("Enter Date in 'YYYY/MM/DD' format: ");
		String dateString = keyboard.nextLine();
		System.out.println(dateString);

		return dateString;
	}

	private int[] splitDate(String dateString) {
		String[] dateSplit = dateString.split("/");
		int[] dateInt = new int[dateSplit.length];

		for (int i = 0; i < dateSplit.length; i++) {
			dateInt[i] = Integer.parseInt(dateSplit[i]);
		}

		return dateInt;
	}

	private void printDate(int[] dateInt) {
		for (int i = 0; i < dateInt.length; i++) {
			switch (i) {
			case 0:
				System.out.println("Year is: " + dateInt[i]);
				break;
			case 1:
				System.out.println("Month is: " + dateInt[i]);
				break;
			case 2:
				System.out.println("Day is: " + dateInt[i]);
				break;
			}
		}

	}
}
