package dowhileloops;

public class DoLoop {

	public static void main(String[] args) {
		int x = 0;
		int max = 10;
		while (x <= max) {
			System.out.println(x);
			x++;
		}
	}
}
