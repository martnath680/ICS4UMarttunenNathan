package dowhileloops;

public class DoWhileLoop3 {
	public static void main(String[] args) {
		int i = 'A';
		while (i <= 'Z') {
			System.out.println((char) i);
			i++;
		}
	}
}
