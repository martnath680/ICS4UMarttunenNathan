package dowhileloops;

import java.util.Random;
import java.util.Scanner;

public class DoWhileLoop2 {

	final static String[] wordCorrect = { "hello", "computer", "pizza" };

	public static void main(String[] args) {
		Scanner keyboard = new Scanner(System.in);
		Random rand = new Random();

		int numb = rand.nextInt(wordCorrect.length);
		// System.out.println(wordCorrect[numb]);

		String wordGuess;

		do {
			System.out.println("Guess Password:");
			wordGuess = keyboard.nextLine();
		} while (!wordGuess.equals(wordCorrect[numb]));

		System.out.println("Correct");
		keyboard.close();
	}
}
