package dowhileloops;

import java.util.Random;
import java.util.Scanner;

public class DoWhileLoop1 {

	public static void main(String[] args) {
		Scanner keyboard = new Scanner(System.in);
		Random rand = new Random();

		int numbCorrect = rand.nextInt(11);
		// System.out.println(numbCorrect);

		int numbGuess;

		do {
			System.out.println("Guess Int from 0 to 10:");
			numbGuess = keyboard.nextInt();
		} while (numbGuess != numbCorrect);

		System.out.println("Correct");
		keyboard.close();
	}
}
