package array2d;

public class Array2dOps {

	private static final int N = 3, M = 5;
	private static int[][] array = new int[N][M];

	public static void main(String[] args) {
		Array2dOps a2d = new Array2dOps();
		a2d.init1();
		a2d.printArray();

		System.out.println();

		a2d.initForLoop();
		a2d.printArray();

	}

	private void init1() {
		array = new int[][] { { 1, 2, 3, 4, 5 }, { 2, 4, 6, 8, 10 }, { 3, 6, 9, 12, 15 } };
	}

	private void initForLoop() {
		for (int i = 0; i < N; i++) {
			for (int j = 0; j < M; j++) {
				array[i][j] = i * 10 + j + 11;
			}
		}
	}

	private void printArray() {
		for (int i = 0; i < N; i++) {
			for (int j = 0; j < M; j++) {
				System.out.print(array[i][j] + " ");
			}
			System.out.println();
		}
	}

}
