package typesizes;

public class TypeSize {

	public static void main(String[] arg) {

		TypeSize.biggestInt();
		TypeSize.biggestDouble();
		TypeSize.smallestDouble();
	}

	private static void biggestInt() {
		System.out.println("=== biggest int ===");
		int i = 1;
		while (i < i + 1) { // does this line make sense?
			i = i + 1;
		}
		System.out.println("biggest int is " + i);
		// System.out.println(Integer.MAX_VALUE);
	}

	private static void biggestDouble() {
		System.out.println("=== biggest double ===");
		double i = 1;
		while (i == (i * 2) - i) {
			i = i * 2;
		}
		System.out.println("biggest double is " + i);
		// System.out.println(Double.MAX_VALUE);
	}

	private static void smallestDouble() {
		System.out.println("=== smallest double ===");
		double epsilon = 1;
		while (1.0 != 1 + epsilon) {
			epsilon = epsilon * 0.999999;
		}
		System.out.println("double epsilon = " + epsilon);
		// System.out.println(Double.MIN_VALUE);
	}

}
