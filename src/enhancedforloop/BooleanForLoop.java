package enhancedforloop;

import java.util.Random;

public class BooleanForLoop {

	boolean[] array = new boolean[100];
	static Random r;

	public static void main(String[] args) {
		BooleanForLoop bfl = new BooleanForLoop();
		r = new Random();

		bfl.initArray();
		bfl.iterateArray();

	}

	private void iterateArray() {
		boolean pre = false;
		for (boolean i : array) {
			System.out.println(i);
			if (pre == true && i == true) {
				System.out.println("Two True");
			}
			pre = i;
		}

	}

	private void initArray() {
		for (int i = 0; i < array.length; i++) {
			array[i] = r.nextBoolean();
		}

	}

}
