package random;

import java.util.Random;

public class RandomInt {
	public static void main(String args[]) {
		Random randomGenerator = new Random();
		int N = 20;
		int low = -40; // This is the lowest number that will be generated
		int high = 35; // This is one higher than the highest number that will be generated
		int[] array = new int[N];

		for (int i = 0; i < N; ++i) {
			int rand = randomGenerator.nextInt(high - low) + low; // gets a value between low <= rand < high
			array[i] = rand;
			System.out.print(rand + " ");
		}

//		System.out.println("");
//
//		for (int i = 0; i < N; ++i) {
//			System.out.print(array[i] + " ");
//		}
	}
}
