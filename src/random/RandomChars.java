package random;

import java.util.Random;

public class RandomChars {

	int N = 20;
	char[] charArray = new char[N];

	public static void main(String args[]) {
		RandomChars randomChars = new RandomChars();

		randomChars.initialize();
		randomChars.print();

	}

	// print out the charArray
	private void print() {
		for (int i = 0; i < charArray.length; i++) {
			System.out.print(charArray[i] + " ");
		}
	}

	// set the elements of charArray to a random
	// char between 'A' and 'Z'
	private void initialize() {
		Random r = new Random();
		int a = 'A';
		int z = 'Z';

		for (int i = 0; i < charArray.length; i++) {
			charArray[i] = (char) (r.nextInt(z - a) + a);
		}
	}
}
