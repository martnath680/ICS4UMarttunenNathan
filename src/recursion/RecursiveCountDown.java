package recursion;

public class RecursiveCountDown {

	public static void main(String[] args) {
		RecursiveCountDown rcd = new RecursiveCountDown();
		rcd.countDown(10, -15);
	}

	public void countDown(int i, int end) {
		if (i < end) {
			return;
		}
		System.out.println(i);
		countDown(i - 1, end);
	}

}
