package icsfileio;

import java.io.FileWriter;
import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.ArrayList;
import java.util.stream.Stream;

public class Ics4uFileIo {

	private ArrayList<String> strings = new ArrayList<String>();
	private Path filePath = Paths.get("src/icsfileio/ics4u.txt");
	private String text = "a\nbb\nccc\ndddd\neeeee\nffffff\nggggggg\nhhhhhhhh\niiiiiiiii\njjjjjjjjjj";

	public static void main(String[] args) throws IOException {
		Ics4uFileIo fio = new Ics4uFileIo();
		fio.write();
		fio.read();
		fio.print();
	}

	private void write() throws IOException {
		FileWriter fw = new FileWriter(filePath.toString());
		fw.write(text);
		fw.close();
	}

	private void read() throws IOException {
		Stream<String> lines = Files.lines(filePath);
		lines.forEach(strings::add);
		lines.close();

	}

	private void print() {
		for (String s : strings) {
			System.out.println(s);
		}
	}
}