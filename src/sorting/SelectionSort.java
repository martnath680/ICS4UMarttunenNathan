package sorting;

import java.util.Random;

/*
 * This form of sorting finds the lowest value in the array, then moves it to the beginning.
 * Then it finds the second lowest value, then swaps it so its in seconds place.
 * This continues until the whole array has been looped through, and all the values are in order.
 * In general, this sorting finds the nth lowest value, and moves it to the nth place in the array,
 * where n is a value from 1 to the array's length.
 */
public class SelectionSort {

	/*
	 * Initialize array with random values between 1 and 100 using for loop
	 */
	private void initialize(int[] array) {
		Random r = new Random();
		for (int i = 0; i < array.length; i++) {
			array[i] = r.nextInt(100) + 1;
		}
	}

	/*
	 * Swap index a and b in array using a temporary variable to store value of a
	 */
	private void swap(int[] array, int a, int b) {
		int temp = array[a];
		array[a] = array[b];
		array[b] = temp;

	}

	/*
	 * Print out array using for loop
	 */
	private void print(int[] array) {
		for (int i = 0; i < array.length; i++) {
			System.out.print(array[i] + " ");
		}
		System.out.println();
	}

	/*
	 * Method for sorting the array. Loops through whole array and finds lowest
	 * value, and moves it to the front of the array. Continue until the whole array
	 * has been looped through
	 */
	private void sort(int[] array) {
		for (int i = 0; i < array.length - 1; i++) { // Loop through all except last index of array
			int curMinVal = array[i]; // Set current index value to temporary lowest value
			int curMinIdx = i; // Set current index to temporary lowest value's index
			for (int j = i + 1; j < array.length; j++) { // Loop through all indexes after current index
				if (array[j] < curMinVal) { // If current is lower than the temporary lowest value
					curMinVal = array[j]; // Set current value to lowest value
					curMinIdx = j; // Set current index to lowest value's index
				}
			}
			swap(array, i, curMinIdx); // After finding lowest value, swap current index with lowest index
		}
	}

	public static void main(String[] args) {
		SelectionSort selectionSort = new SelectionSort();
		int[] array = new int[20];
		selectionSort.initialize(array);
		selectionSort.print(array);
		selectionSort.sort(array);
		selectionSort.print(array);
	}

}
