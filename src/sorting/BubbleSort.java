package sorting;

import java.util.Random;

/*
 * Bubble Sort is a sorting algorithm that 'bubbles' the largest 
 * number to the end of the array. The first comparison is 
 * index 0 and 1, and if 0 > 1, they swap places. Then,
 * index 1 and 2 are compared, swapping if needed, and repeating the steps 
 * until the end of the array is reached. These steps are repeated over
 * until the whole array is in order, and all the larger numbers have 
 * 'bubbled' or swapped their way to the end of the array.
 */
public class BubbleSort {
	/*
	 * Initialize array with random values between 1 and 100 using for loop
	 */
	private void initialize(int[] array) {
		Random r = new Random();
		for (int i = 0; i < array.length; i++) {
			array[i] = r.nextInt(100) + 1;
		}
	}

	/*
	 * Swap index a and b in array using a temporary variable to store value of a
	 */
	private void swap(int[] array, int a, int b) {
		int temp = array[a];
		array[a] = array[b];
		array[b] = temp;

	}

	/*
	 * Print out array using for loop
	 */
	private void print(int[] array) {
		for (int i = 0; i < array.length; i++) {
			System.out.print(array[i] + " ");
		}
		System.out.println();
	}

	/*
	 * Bubble Sort from Description Online
	 */
	public void sort(int[] array) {
		boolean swapped;
		int endOfArray = 1; // Every iteration, more of the end of the array will be sorted, so redundant
							// to check and sort again
		do {
			swapped = false;
			for (int i = 0; i < array.length - endOfArray; i++) {
				if (array[i + 1] < array[i]) {
					swap(array, i, i + 1);
					swapped = true;
				}
			}
			endOfArray++;
		} while (swapped);
	}

	/*
	 * Bubble Sort from Description on Fuller's Assignment
	 */
	public void sort2(int[] array) {
		for (int i = 0; i < array.length - 2; i++) {
			for (int j = i + 1; j < array.length - 1; j++) {
				if (array[j] < array[i]) {
					swap(array, j, i);
				}
			}
		}

	}

	public static void main(String[] args) {
		BubbleSort bubbleSort = new BubbleSort();
		int[] array = new int[20];
		bubbleSort.initialize(array);
		bubbleSort.print(array);
		bubbleSort.sort(array);
		// bubbleSort.sort2(array);
		bubbleSort.print(array);
	}
}
